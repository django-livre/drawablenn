package drawable.fatec.com.drawablenn.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import drawable.fatec.com.drawablenn.R;
import drawable.fatec.com.drawablenn.background.RegisterCallback;
import drawable.fatec.com.drawablenn.model.RegisterResponse;
import drawable.fatec.com.drawablenn.model.User;
import drawable.fatec.com.drawablenn.mvp.MVP;
import drawable.fatec.com.drawablenn.presenter.FactualPresenterLogin;
import drawable.fatec.com.drawablenn.view.util.FormValidator;

/**
 * Classe da janela de controle de usuário
 */
public class RegistryActivity extends AppCompatActivity implements MVP.ViewRegistry, RegisterCallback {

    @BindView(R.id.editTextUserID)
    EditText username;
    @BindView(R.id.editTextUserEmail)
    EditText email;
    @BindView(R.id.editTextUserPasswordOne)
    EditText passwordOne;
    @BindView(R.id.editTextUserPasswordTwo)
    EditText passwordTwo;
    @BindView(R.id.baconImage)
    ImageView imageView;

    private MVP.PresenterLogin presenterLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_registry);
        ButterKnife.bind(this);
        imageView.setImageResource(R.drawable.airplane);
        presenterLogin = new FactualPresenterLogin(this);
    }

    @Override
    public void registerUser(View view) {

        if (FormValidator.validate(username, email, passwordOne, passwordTwo)) {
            if (passwordOne.getText().toString().equals(passwordTwo.getText().toString())) {
                presenterLogin.registerUser(new User(username.getText().toString(),
                        passwordOne.getText().toString(), email.getText().toString()));

            } else {
                MessageUI.toastMessage(this, "As senhas são diferentes");
            }
        }

    }

    @Override
    public void backToLogin() {

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);

    }

    @Override
    public void doRegistry(RegisterResponse registerResponse) {

        if (registerResponse != null) {
            if (registerResponse.isSuccess()) {
                MessageUI.toastMessage(this, "Usuário cadastrado com sucesso");
                backToLogin();
            } else {
                MessageUI.toastMessage(this, "Problema ao cadastrar o usuário. " +
                        "Tente novamente mais tarde");
            }
        } else {
            MessageUI.toastMessage(this, "Usuário inválido, tente outro nome de usuário");
        }

    }
}
