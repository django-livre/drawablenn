package drawable.fatec.com.drawablenn.view;

import android.content.Intent;
import android.graphics.drawable.Icon;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import butterknife.BindView;
import butterknife.ButterKnife;
import drawable.fatec.com.drawablenn.R;
import drawable.fatec.com.drawablenn.background.LoginCallback;
import drawable.fatec.com.drawablenn.model.LoginResponse;
import drawable.fatec.com.drawablenn.model.User;
import drawable.fatec.com.drawablenn.mvp.MVP;
import drawable.fatec.com.drawablenn.presenter.FactualPresenterLogin;
import drawable.fatec.com.drawablenn.view.util.FormValidator;

public class LoginActivity extends AppCompatActivity implements MVP.ViewLogin, LoginCallback {

    @BindView(R.id.editTextUserLogin)
    EditText username;
    @BindView(R.id.editTextUserPassword)
    EditText password;
    @BindView(R.id.imageView)
    ImageView imageView;

    private MVP.PresenterLogin presenterLogin;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        imageView.setImageResource(R.drawable.icon);
        presenterLogin = new FactualPresenterLogin(this);
    }

    @Override
    public void login(View view) {

        user = new User(username.getText().toString(), password.getText().toString());

        if (FormValidator.validate(username, password)) {
            presenterLogin.loginUser(user);
        }
    }

    @Override
    public void registerUser(View view) {

        Intent intent = new Intent(this, RegistryActivity.class);
        startActivity(intent);

    }

    @Override
    public void changeView() {

        Intent intent = new Intent(this, MenuActivity.class);
        intent.putExtra("user", user);

        startActivity(intent);
        // finish();
    }

    @Override
    public void doLogin(LoginResponse loginResponse) {

        if (loginResponse != null) {
            if (loginResponse.isLogged()) {
                MessageUI.toastMessage(this, "Login efetuado com sucesso!");
                changeView();
            } else {
                MessageUI.toastMessage(this, "Ops! Não foi possível realizar o login");
            }
        } else {
            MessageUI.toastMessage(this, "Usuário ou senha incorretos");
        }
    }
}

