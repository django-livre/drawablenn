package drawable.fatec.com.drawablenn.neural;

import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Bitmap;

import org.tensorflow.lite.Interpreter;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

import static drawable.fatec.com.drawablenn.image.ImageProcessor.bitmapToByteBuffer;
import static drawable.fatec.com.drawablenn.neural.ClassifierInformation.CATEGORY_COUNT;

/**
 * Classificador de imagens
 * Código criado com base nos exemplos da documentação do Tensorflow Lite
 */
public class Classifier {
    private final String MODEL_PATH = "model_tflite.tflite";

    private Interpreter mInterpreter;

    public Classifier(AssetManager assetManager) throws IOException {
        mInterpreter = new Interpreter(loadModelFile(assetManager));
    }

    /**
     * Método para classificar imagens Bitmap
     *
     * @param bitmap
     * @return
     */
    public ClassifierResult classify(Bitmap bitmap) {

        float[][] mResult = new float[1][CATEGORY_COUNT];

        mInterpreter.run(bitmapToByteBuffer(bitmap), mResult);
        return new ClassifierResult(mResult[0]);
    }

    /**
     * Método que carrega o modelo (.tflite) da rede neural
     *
     * @param assetManager
     * @return
     * @throws IOException
     */
    private MappedByteBuffer loadModelFile(AssetManager assetManager) throws IOException {
        AssetFileDescriptor fileDescriptor = assetManager.openFd(MODEL_PATH);
        FileInputStream inputStream = new FileInputStream(fileDescriptor.getFileDescriptor());
        FileChannel fileChannel = inputStream.getChannel();

        long startOffset = fileDescriptor.getStartOffset();
        long declaredLength = fileDescriptor.getDeclaredLength();

        return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength);
    }
}
