package drawable.fatec.com.drawablenn.background;

import android.os.AsyncTask;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import drawable.fatec.com.drawablenn.factories.RetrofitFactory;
import drawable.fatec.com.drawablenn.model.PointResponse;
import drawable.fatec.com.drawablenn.model.User;
import drawable.fatec.com.drawablenn.mvp.MVP;
import retrofit2.Call;
import retrofit2.Response;

public class PointProcess extends AsyncTask<Integer, Void, PointResponse> {

    private MVP.Model retrofitModel;
    private PointCallback pointCallback;
    private User user;
    private Integer point;

    public PointProcess(PointCallback pointCallback, User currentUser, Integer point) {
        this.retrofitModel = RetrofitFactory.retrofitFactory();
        this.pointCallback = pointCallback;
        this.user = currentUser;
        this.point = point;

    }

    @Override
    protected PointResponse doInBackground(Integer... integers) {

        PointResponse pointResponse = null;

        try {

            Call<PointResponse> call = retrofitModel.savePoint(user.getId(), point);
            Response<PointResponse> executeResponse = call.execute();
            pointResponse = executeResponse.body();

        } catch (Exception e) {

            Log.e("Request error, could not be possible add points: ", e.getMessage());
        }

        return pointResponse;
    }

    @Override
    protected void onPostExecute(PointResponse pointResponse) {

        pointCallback.savePoint(pointResponse);

    }
}
