package drawable.fatec.com.drawablenn.background;

import android.os.AsyncTask;
import android.util.Log;

import drawable.fatec.com.drawablenn.factories.RetrofitFactory;
import drawable.fatec.com.drawablenn.model.ProfileResponse;
import drawable.fatec.com.drawablenn.model.User;
import drawable.fatec.com.drawablenn.mvp.MVP;
import retrofit2.Call;
import retrofit2.Response;

public class ProfileProcess extends AsyncTask<User, Void, ProfileResponse> {

    private MVP.Model retrofitModel;
    private ProfileCallback profileCallback;

    public ProfileProcess(ProfileCallback profileCallback) {
        this.retrofitModel = RetrofitFactory.retrofitFactory();
        this.profileCallback = profileCallback;
    }

    @Override
    protected ProfileResponse doInBackground(User... users) {

        ProfileResponse profileResponse = null;
        Call<ProfileResponse> call = retrofitModel.userProfile(users[0].getId());

        try {
            Response executeResponse = call.execute();
            profileResponse = (ProfileResponse) executeResponse.body();
        } catch (Exception e) {
            Log.d("LoginError", e.getMessage());
        }
        return profileResponse;
    }

    @Override
    protected void onPostExecute(ProfileResponse profileResponse) {
        profileCallback.showProfile(profileResponse);
    }
}
