package drawable.fatec.com.drawablenn.background;

import android.os.AsyncTask;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import drawable.fatec.com.drawablenn.model.LoginResponse;
import drawable.fatec.com.drawablenn.factories.RetrofitFactory;
import drawable.fatec.com.drawablenn.model.User;
import drawable.fatec.com.drawablenn.mvp.MVP;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class LoginProcess extends AsyncTask<User, Void, LoginResponse> {

    private MVP.Model retrofitModel;
    private LoginCallback loginCallback;

    public LoginProcess(LoginCallback loginCallback) {

        this.retrofitModel = RetrofitFactory.retrofitFactory();
        this.loginCallback = loginCallback;

    }

    @Override
    protected LoginResponse doInBackground(User[] user) {

        LoginResponse loginResponse = null;
        Map<String, String> map = new HashMap<>();
        map.put("id", user[0].getId());
        map.put("password", user[0].getPassword());

        try {

            Call<LoginResponse> call = retrofitModel.loginUser(map);
            Response executeResponse = call.execute();
            loginResponse = (LoginResponse) executeResponse.body();

        } catch (Exception e) {

            Log.d("LoginError", e.getMessage());

        }
        return loginResponse;
    }

    @Override
    protected void onPostExecute(LoginResponse loginResponse) {

        loginCallback.doLogin(loginResponse);

    }
}
