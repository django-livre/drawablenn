package drawable.fatec.com.drawablenn.presenter;

import drawable.fatec.com.drawablenn.background.ProfileCallback;
import drawable.fatec.com.drawablenn.background.ProfileProcess;
import drawable.fatec.com.drawablenn.factories.RetrofitFactory;
import drawable.fatec.com.drawablenn.model.User;
import drawable.fatec.com.drawablenn.mvp.MVP;

public class FactualPresenterProfile implements MVP.PresenterProfile {

    private ProfileCallback profileCallback;

    public FactualPresenterProfile(ProfileCallback profileCallback) {
        this.profileCallback = profileCallback;
    }

    @Override
    public void getUserProfile(User user) {
        ProfileProcess profileProcess = new ProfileProcess(profileCallback);
        profileProcess.execute(user);
    }
}