package drawable.fatec.com.drawablenn.neural;

import drawable.fatec.com.drawablenn.neural.ClassifierInformation;

/**
 * Resultado da classificação da rede neural.
 */
public class ClassifierResult {

    private int classNumber;
    private String className;
    private float mProbability;

    public ClassifierResult(float[] result) {
        classNumber = argmax(result);
        mProbability = result[classNumber];
        className = ClassifierInformation.CLASSES[classNumber];
    }

    /**
     * Recupera o nome da classe da imagem
     *
     * @return
     */
    public String getClassName() {
        return className;
    }

    /**
     * Recupera o número da classe da imagem
     *
     * @return
     */
    public int getClassNumber() {
        return classNumber;
    }

    /**
     * Recupera a probabilidade obtida pela classe
     *
     * @return
     */
    public float getProbability() {
        return mProbability;
    }

    /**
     * Método para recuperar o maior valor nas probabilidades, para então,
     * realizar a classificação
     *
     * @param probs
     * @return
     */
    private static int argmax(float[] probs) {
        int maxIdx = -1;
        float maxProb = 0.0f;

        for (int i = 0; i < probs.length; i++) {
            if (probs[i] > maxProb) {
                maxProb = probs[i];
                maxIdx = i;
            }
        }
        return maxIdx;
    }
}
