package drawable.fatec.com.drawablenn.game;

import android.util.Log;

import java.util.TimerTask;

/**
 * A Action to be exec in GameTime finish
 */
public class GameTimeAction extends TimerTask {

    private GameTime gameTime;

    public GameTimeAction(GameTime gameTime) {
        this.gameTime = gameTime;
    }

    @Override
    public void run() {
        try {
            this.gameTime.setTimeout(true);
        } catch (RuntimeException e) {
            Log.e("GameTimeAction", e.getMessage());
        }
    }
}
