package drawable.fatec.com.drawablenn.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import drawable.fatec.com.drawablenn.R;
import drawable.fatec.com.drawablenn.background.ProfileCallback;
import drawable.fatec.com.drawablenn.model.ProfileResponse;
import drawable.fatec.com.drawablenn.model.User;
import drawable.fatec.com.drawablenn.mvp.MVP;
import drawable.fatec.com.drawablenn.presenter.FactualPresenterProfile;

public class ProfileActivity extends AppCompatActivity implements MVP.ViewUserProfile, ProfileCallback {

    @BindView(R.id.profileUserEmail)
    TextView profileUserEmail;
    @BindView(R.id.profileUserName)
    TextView profileUserName;
    @BindView(R.id.profileUserPoints)
    TextView profileUserPoints;
    @BindView(R.id.profileImageView)
    ImageView profileImageView;

    private MVP.PresenterProfile presenterProfile;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);

        profileImageView.setImageResource(R.drawable.profile);
        user = (User) getIntent().getSerializableExtra("user");
        presenterProfile = new FactualPresenterProfile(this);

        seeUserProfile();
    }

    @Override
    public void seeUserProfile() {
        presenterProfile.getUserProfile(user);
    }

    @Override
    public void showProfile(ProfileResponse profileResponse) {
        if (profileResponse != null) {
            profileUserEmail.setText(profileResponse.getEmail());
            profileUserName.setText(profileResponse.getName());
            profileUserPoints.setText(profileResponse.getPoits().toString());
        } else {
            MessageUI.toastMessage(this, "Erro ao tentar recuperar o perfil");
        }
    }
}
