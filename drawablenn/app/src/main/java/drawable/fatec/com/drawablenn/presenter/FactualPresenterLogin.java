package drawable.fatec.com.drawablenn.presenter;

import android.util.Log;

import drawable.fatec.com.drawablenn.background.LoginCallback;
import drawable.fatec.com.drawablenn.background.LoginProcess;
import drawable.fatec.com.drawablenn.background.RegisterCallback;
import drawable.fatec.com.drawablenn.background.RegisterProcess;
import drawable.fatec.com.drawablenn.model.User;
import drawable.fatec.com.drawablenn.mvp.MVP;
import drawable.fatec.com.drawablenn.view.MenuActivity;

/**
 * Presenter da classe de Login
 */
public class FactualPresenterLogin implements MVP.PresenterLogin {

    private LoginCallback loginCallback;
    private RegisterCallback registerCallback;

    public FactualPresenterLogin(LoginCallback loginCallback) {
        this.loginCallback = loginCallback;
    }

    public FactualPresenterLogin(RegisterCallback registerCallback) {
        this.registerCallback = registerCallback;
    }

    @Override
    public void loginUser(User user) {
        LoginProcess loginProcess = new LoginProcess(loginCallback);
        loginProcess.execute(user);
    }

    @Override
    public void registerUser(User user) {
        RegisterProcess registerProcess = new RegisterProcess(registerCallback);
        registerProcess.execute(user);
    }
}
