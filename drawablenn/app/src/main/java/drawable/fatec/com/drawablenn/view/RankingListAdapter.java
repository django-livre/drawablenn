package drawable.fatec.com.drawablenn.view;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.jpardogo.android.flabbylistview.lib.FlabbyLayout;

import java.util.Collections;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import drawable.fatec.com.drawablenn.R;
import drawable.fatec.com.drawablenn.model.User;

public class RankingListAdapter extends ArrayAdapter<User> {

    private Context mContext;
    private List<User> mItems;
    private Random mRandomizer = new Random();

    public RankingListAdapter(Context context, int simple_list_item_1, List<User> items) {
        super(context, 0, items);
        mContext = context;
        mItems = items;
        Collections.sort(mItems);
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public User getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_list, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        int color = Color.argb(255, mRandomizer.nextInt(256), mRandomizer.nextInt(256), mRandomizer.nextInt(256));
        ((FlabbyLayout)convertView).setFlabbyColor(color);
        holder.text.setText(getItem(position).toString());
        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.text)
        TextView text;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
