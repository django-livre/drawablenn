package drawable.fatec.com.drawablenn.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import butterknife.ButterKnife;
import drawable.fatec.com.drawablenn.R;
import drawable.fatec.com.drawablenn.model.User;
import drawable.fatec.com.drawablenn.mvp.MVP;

public class MenuActivity extends AppCompatActivity implements MVP.ViewPrincipalMenu {

    private User user;
    private MVP.PresenterLogin factualPresenterLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        ButterKnife.bind(this);

        user = (User) getIntent().getSerializableExtra("user");
    }

    @Override
    public void play(View view) {
        Intent intent = new Intent(this, GameActivity.class);
        intent.putExtra("user",user);
        startActivity(intent);
        //finish();
    }

    @Override
    public void seeRanking(View view) {
        Intent intent = new Intent(this, RankingActivity.class);
        startActivity(intent);
    }

    @Override
    public void seeProfile(View view) {
        Intent intent = new Intent(this, ProfileActivity.class);
        intent.putExtra("user", user);

        startActivity(intent);
    }

    @Override
    public void quitApp(View view) {
        MessageUI.toastMessage(this, "Até mais");
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
