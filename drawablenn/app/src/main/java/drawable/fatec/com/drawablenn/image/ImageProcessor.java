package drawable.fatec.com.drawablenn.image;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class ImageProcessor {

    public static final int DIM_IMG_HEIGHT = 28;
    public static final int DIM_IMG_WIDTH = 28;

    public static final int DIM_BATCH_SIZE = 1;
    public static final int DIM_PIXEL_SIZE = 1;


    private static final ColorFilter COLOR_FILTER = new ColorMatrixColorFilter(new ColorMatrix(
            new float[]{
                    -1, 0, 0, 0, 255,
                    0, -1, 0, 0, 255,
                    0, 0, -1, 0, 255,
                    0, 0, 0, 1, 0
            }));

    private ImageProcessor() {
    }

    /**
     * Método para converter a imagem Bitmap em Buffer
     *
     * @param bitmap
     */
    public static ByteBuffer bitmapToByteBuffer(Bitmap bitmap) {

        int[] mImagePixels = new int[DIM_IMG_HEIGHT * DIM_IMG_WIDTH];
        bitmap = transpose(bitmap);

        ByteBuffer mImageData = ByteBuffer.allocateDirect(
                4 * DIM_BATCH_SIZE * DIM_IMG_HEIGHT * DIM_IMG_WIDTH * DIM_PIXEL_SIZE
        );
        mImageData.order(ByteOrder.nativeOrder());
        mImageData.rewind();

        bitmap.getPixels(mImagePixels, 0, bitmap.getWidth(), 0, 0,
                bitmap.getWidth(), bitmap.getHeight());

        int pixel = 0;
        for (int i= 0; i < DIM_IMG_WIDTH; i++) {
            for (int j = 0; j < DIM_IMG_HEIGHT; j++) {
                final int val = mImagePixels[pixel++];
                final float grayScale = convertToGreyScale(val);
                mImageData.putFloat(grayScale);
            }
        }
        return mImageData;
    }

    /**
     * Método para transformar um pixel em tons de cinza
     * @param pixel
     * @return
     */
    private static float convertToGreyScale(int pixel) {
        return (((pixel >> 16) & 0xFF) + ((pixel >> 8) & 0xFF) + (pixel & 0xFF)) / 3.0f / 255.0f;
    }

    /**
     * Método para inverter as cores da imagem Bitmap
     *
     * @param bitmap
     * @return
     */
    private static Bitmap transpose(Bitmap bitmap) {
        Bitmap inverted = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(inverted);
        Paint paint = new Paint();
        paint.setColorFilter(COLOR_FILTER);
        canvas.drawBitmap(bitmap, 0, 0, paint);
        return inverted;
    }
}
