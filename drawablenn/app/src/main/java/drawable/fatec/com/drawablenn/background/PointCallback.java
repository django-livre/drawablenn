package drawable.fatec.com.drawablenn.background;

import drawable.fatec.com.drawablenn.model.PointResponse;

public interface PointCallback {
    void savePoint(PointResponse pointResponse);
}
