package drawable.fatec.com.drawablenn.game;

import android.util.Log;

/**
 * Class to control the User gain points in match
 */
public class PointManager {

    private Integer classifiedEightyPercent;
    private Integer classifiedSixtyPercent;

    public PointManager() {
        classifiedEightyPercent = 0;
        classifiedSixtyPercent = 0;
    }

    public void assertPoint(float probability) {
        if (probability >= 0.80) {
            classifiedEightyPercent += 1;
        } else if (probability >= 0.60) {
            classifiedSixtyPercent += 1;
        }
    }

    public Integer validatePoint() {

        if (classifiedSixtyPercent == 2) {
            classifiedSixtyPercent = 0;
            return 10;
        } else if (classifiedEightyPercent == 2) {
            classifiedEightyPercent = 0;
            return 5;
        } else
            return 0;
    }

    private Integer assertPowerUps() {
        return 0;
    }

}
