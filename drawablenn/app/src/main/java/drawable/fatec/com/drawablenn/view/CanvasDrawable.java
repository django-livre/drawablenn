package drawable.fatec.com.drawablenn.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;

/**
 * A View Widget for the user to draw
 */
public class CanvasDrawable extends View {

    private Path path;
    private Paint drawingPaint;
    private Paint textPaint;
    private String classText;
    private String timerText;
    private String heartPointsText;

    public CanvasDrawable(Context context) {
        super(context);
        init();
    }

    public CanvasDrawable(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public CanvasDrawable(Context context, AttributeSet attributeSet, int defStyle) {
        super(context, attributeSet, defStyle);
        init();
    }

    /**
     * Method to configure canvas elements
     */
    public void init() {
        path = new Path();
        // Configure the Paint drawing
        drawingPaint = new Paint();
        drawingPaint.setAntiAlias(true);
        drawingPaint.setStyle(Paint.Style.STROKE);
        drawingPaint.setStrokeWidth(40F);
        drawingPaint.setStrokeCap(Paint.Cap.ROUND);
        drawingPaint.setStrokeJoin(Paint.Join.MITER);
        drawingPaint.setColor(Color.BLACK);

        // Configure the Paint text
        textPaint = new Paint();
        textPaint.setTextSize(40);
        textPaint.setColor(Color.BLACK);
        textPaint.setStyle(Paint.Style.FILL);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawPath(path, drawingPaint);
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);

        DisplayMetrics displayMetrics = this.getContext().getResources().getDisplayMetrics();

        Float x = ((Double) (displayMetrics.xdpi * 1.0)).floatValue();
        Float y = ((Double) (displayMetrics.ydpi * 1.0)).floatValue();

        canvas.drawText(classText, x / 10, y / 4, textPaint);
        canvas.drawText(heartPointsText, x / 10, (y / 4) + (textPaint.getTextSize()), textPaint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        float xPos = event.getX();
        float yPos = event.getY();

        switch (event.getAction()) {
            // Touch event
            case MotionEvent.ACTION_DOWN:
                path.moveTo(xPos, yPos);
                return true;
            // Touch Dragged
            case MotionEvent.ACTION_MOVE:
                path.lineTo(xPos, yPos);
                break;
            // UnTouch
            case MotionEvent.ACTION_UP:
                break;
            default:
                return false;
        }
        invalidate();
        return true;
    }

    /**
     * Method to get Scaled Bitmap from Canvas
     *
     * @param width  Image width
     * @param height image height
     * @return A bitmap scaled image
     */
    public Bitmap getScaledBitmap(int width, int height) {
        // Update the canvas frame
        this.setDrawingCacheEnabled(false);
        this.setDrawingCacheEnabled(true);

        return Bitmap.createScaledBitmap(this.getDrawingCache(), width, height, false);
    }

    /**
     * Method to clear canvas
     */
    public void clearCanvas() {
        path = new Path();
        invalidate();
    }

    /**
     * Change the text class in canvas
     *
     * @param classText
     */
    public void setClassText(String classText) {
        this.classText = "Desenhe um(a): " + classText;
    }

    /**
     * Set the user points show in the Canvas
     *
     * @param heartPointsText
     */
    public void setHeartPointsText(Integer heartPointsText) {
        this.heartPointsText = "Vidas: " + heartPointsText;
    }
}
