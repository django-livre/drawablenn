package drawable.fatec.com.drawablenn.game;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.util.Log;

import java.util.Random;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import drawable.fatec.com.drawablenn.neural.Classifier;
import drawable.fatec.com.drawablenn.neural.ClassifierResult;

/**
 * Engine of DrawableNN Game
 */
public class GameEngine {

    private AssetManager assetManager;

    // General game attributes
    private int heartPoints;
    private String[] classes;

    // Tensorflow (tf) attributes
    private Classifier classifier;
    private Executor executor = Executors.newSingleThreadExecutor();

    public GameEngine(AssetManager assetManager, int heartPoints, String[] classes) {
        this.classes = classes;
        this.heartPoints = heartPoints;
        this.assetManager = assetManager;

        // Start the tf model
        initTensorflowAndLoadModel();
    }

    public int getHeartPoints() {
        return this.heartPoints;
    }

    public boolean isAlive() {
        return this.heartPoints > 0;
    }

    /**
     * Method to classify the user Bitmap
     *
     * @param bitmap
     * @return Return the classifier result
     */
    public ClassifierResult classifyBitmap(Bitmap bitmap) {
        if (this.isAlive()) {
            return classifier.classify(bitmap);
        } else {
            throw new RuntimeException("The game has been finished. Create a new session");
        }
    }

    /**
     * Method to get a random class to user classify
     *
     * @return
     */
    public String raffleImageClass() {
        if (this.isAlive()) {
            return classes[new Random().nextInt(classes.length)];
        } else {
            throw new RuntimeException("The game has been finished. Create a new session");
        }
    }

    /**
     * Method to remove a points of a game
     *
     * @return boolean Flag to indicate whether or not the user should remain in the game
     */
    public void removeHeartPoint() {
        if (this.isAlive()) {
            this.heartPoints -= 1;
        } else {
            throw new RuntimeException("The game has been finished. Create a new session");
        }
    }

    /**
     * Load the model and init the Tensorflow interpreter
     */
    private void initTensorflowAndLoadModel() {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    classifier = new Classifier(assetManager);
                } catch (Exception e) {
                    Log.e("TensorFlowLoadError", e.getMessage());
                    throw new RuntimeException(e.getMessage());
                }
            }
        });
    }
}
