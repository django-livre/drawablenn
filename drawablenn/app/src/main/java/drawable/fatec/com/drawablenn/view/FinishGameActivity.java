package drawable.fatec.com.drawablenn.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import butterknife.BindView;
import butterknife.ButterKnife;
import drawable.fatec.com.drawablenn.R;
import drawable.fatec.com.drawablenn.model.User;

public class FinishGameActivity extends AppCompatActivity {

    @BindView(R.id.adView)
    AdView mAdView;
    private User currentUser;
    @BindView(R.id.imageView4)
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish_game);
        currentUser = (User) getIntent().getSerializableExtra("user");

        ButterKnife.bind(this);
        Glide.with(this).load(R.raw.coxinhaemoji).into(imageView);

        MobileAds.initialize(this, "ca-app-pub-4211687768112293~6586296400");
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    /**
     * Method to restart the game view
     * @param view
     */
    public void restartGame(View view) {
        Intent intent = new Intent(this, GameActivity.class);
        intent.putExtra("user", currentUser);
        startActivity(intent);
        finish();
    }

    /**
     * Method to back user to menu activity
     */
    public void backToMenu(View view) {
        Intent intent = new Intent(this, MenuActivity.class);
        intent.putExtra("user", currentUser);
        startActivity(intent);
        finish();
    }
}
