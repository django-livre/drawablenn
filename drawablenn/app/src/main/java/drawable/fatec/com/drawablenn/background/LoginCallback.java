package drawable.fatec.com.drawablenn.background;

import drawable.fatec.com.drawablenn.model.LoginResponse;
import drawable.fatec.com.drawablenn.model.RegisterResponse;

/**
 * Interface de Callback para as ações de login (Acesso)
 */
public interface LoginCallback {
    void doLogin(LoginResponse loginResponse);
}
