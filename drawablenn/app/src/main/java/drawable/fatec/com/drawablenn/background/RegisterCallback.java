package drawable.fatec.com.drawablenn.background;

import drawable.fatec.com.drawablenn.model.RegisterResponse;

/**
 * Interface de Callback para as ações de registro na API
 */
public interface RegisterCallback {
    void doRegistry(RegisterResponse registerResponse);
}
