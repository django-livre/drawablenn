package drawable.fatec.com.drawablenn.background;

import drawable.fatec.com.drawablenn.model.RankingResponse;

public interface RankingCallback {
    void fillRanking(RankingResponse rankingResponse);
}
