package drawable.fatec.com.drawablenn.view;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;

public class MessageUI {
    public static void toastMessage(Context context, String message) {
        Toasty.info(context, message, Toast.LENGTH_SHORT, true).show();
    }
}
