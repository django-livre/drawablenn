package drawable.fatec.com.drawablenn.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import drawable.fatec.com.drawablenn.R;
import drawable.fatec.com.drawablenn.background.RankingCallback;
import drawable.fatec.com.drawablenn.model.RankingResponse;
import drawable.fatec.com.drawablenn.model.User;
import drawable.fatec.com.drawablenn.mvp.MVP;
import drawable.fatec.com.drawablenn.presenter.FactualPresenterRanking;

public class RankingActivity extends AppCompatActivity implements MVP.ViewRanking, RankingCallback {

    @BindView(R.id.viewListOfUsers)
    ListView rankingOfUsers;
    private MVP.PresenterRanking presenterRanking;
    private RankingListAdapter usersOfRanking;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranking);
        ButterKnife.bind(this);

        presenterRanking = new FactualPresenterRanking(this);
        presenterRanking.seeRanking();
    }

    @Override
    public void fillRanking(RankingResponse rankingResponse) {
        List<User> users = new LinkedList<>();
        users.addAll(rankingResponse.getUsers().values());

        usersOfRanking = new RankingListAdapter(this,
                                android.R.layout.simple_list_item_1, users);
        rankingOfUsers.setAdapter(usersOfRanking);
    }
}
