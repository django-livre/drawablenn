package drawable.fatec.com.drawablenn.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Abstração da resposta da API para login de usuário
 */
public class LoginResponse {

    @SerializedName("success")
    @Expose
    private boolean success;

    @SerializedName("description")
    @Expose
    private String description;

    public boolean isLogged() {
        return success;
    }

    public void setSuccess(boolean success) { success = success; }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
