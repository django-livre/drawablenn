package drawable.fatec.com.drawablenn.presenter;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.widget.TextView;

import drawable.fatec.com.drawablenn.background.PointCallback;
import drawable.fatec.com.drawablenn.background.PointProcess;
import drawable.fatec.com.drawablenn.game.GameEngine;
import drawable.fatec.com.drawablenn.game.GameTime;
import drawable.fatec.com.drawablenn.game.PointManager;
import drawable.fatec.com.drawablenn.model.User;
import drawable.fatec.com.drawablenn.mvp.MVP;
import drawable.fatec.com.drawablenn.neural.ClassifierInformation;
import drawable.fatec.com.drawablenn.neural.ClassifierResult;

public class FactualPresenterGame implements MVP.PresenterGame {

    private User user;
    private Integer point;
    private GameTime gameTime;
    private GameEngine gameEngine;
    private String actualDrawClass;
    private boolean gameIsFinished;
    private PointManager pointManager;
    private PointCallback pointCallback;

    public FactualPresenterGame(Context context, PointCallback pointCallback, User user, TextView time) {
        gameEngine = new GameEngine(context.getAssets(), 3, ClassifierInformation.CLASSES);

        this.user = user;
        gameIsFinished = false;
        this.pointCallback = pointCallback;
        this.pointManager = new PointManager();

        gameTime = new GameTime(15, time);
        gameTime.startTimer();
    }

    private void saveUserImage(Float probability, Bitmap bitmap) {
        if (probability > 0.70) {
            // ToDo: Insert the logic using retrofit to save image in Database
        }
    }

    @Override
    public String getActualDrawClass() {
        return actualDrawClass;
    }

    @Override
    public boolean classifyAndCheckUserDraw(Bitmap bitmap) {
        try {
            ClassifierResult classifierResult = gameEngine.classifyBitmap(bitmap);
            saveUserImage(classifierResult.getProbability(), bitmap);

            if (classifierResult.getClassName().equals(this.actualDrawClass)) {
                pointManager.assertPoint(classifierResult.getProbability());
                point = pointManager.validatePoint();
                if (point != 0) {
                    savePoint();
                }
                return true;
            }
            return false;
        } catch (RuntimeException e) {
            gameIsFinished = true;
            Log.e("PresenterGame", e.getMessage());
        }
        return false;
    }

    @Override
    public void changeDrawClass() {
        try {
            this.actualDrawClass = gameEngine.raffleImageClass(); // Save the result
        } catch (RuntimeException e) {
            gameIsFinished = true;
            Log.e("ChangeDrawGame", e.getMessage());
        }
    }

    @Override
    public void removeUserHeartPoint() {
        try {
            this.gameEngine.removeHeartPoint();
        } catch (RuntimeException e) {
            gameIsFinished = true;
            Log.e("RemoveUserHeart", e.getMessage());
        }
    }

    @Override
    public void changeTime(int seconds) {
        gameTime.changeTime(seconds);
    }

    @Override
    public void savePoint() {
        PointProcess pointProcess = new PointProcess(pointCallback, user, point);
        pointProcess.execute();
        point = 0;
    }

    @Override
    public void startGameTime() {
        gameTime.startTimer();
    }

    @Override
    public boolean isTimeout() {
        return gameTime.isTimeout();
    }

    @Override
    public boolean playerIsAlive() {
        return gameEngine.isAlive();
    }

    @Override
    public int seeUserHeartPoint() {
        return gameEngine.getHeartPoints();
    }

    @Override
    public int seeUserTimeFreezePowerup() {
        return 0;
    }

    @Override
    public boolean gameIsFinished() {
        return gameIsFinished;
    }
}
