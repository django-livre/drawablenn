package drawable.fatec.com.drawablenn.background;

import drawable.fatec.com.drawablenn.model.ProfileResponse;

public interface ProfileCallback {

    /**
     * Método para inserir na activity os dados do perfil do usuário
     * @param profileResponse
     */
    void showProfile(ProfileResponse profileResponse);

}
