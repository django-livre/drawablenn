package drawable.fatec.com.drawablenn.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Classe de usuário
 */
public class User implements Serializable, Comparable<User> {

    @SerializedName("points")
    @Expose
    private Integer points;
    @SerializedName("position")
    @Expose
    private Integer rankingPosition;
    @SerializedName("name")
    @Expose
    private String name;
    private String id;
    private String password;
    private String email;

    public User(String id, String password) {
        this.id = id;
        this.name = id;
        this.password = password;
    }

    public User(String id, String password, String email) {
        this.id = id;
        this.name = id;
        this.password = password;
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public Integer getRankingPosition(){
        return rankingPosition;
    }

    @Override
    public String toString() {
        if (points != null && rankingPosition != null) {
            return "Nome: " + name + "| Pontos: " + points;
        } else {
            return "Nome: " + name;
        }
    }

    @Override
    public int compareTo(User o) {
        if (this.rankingPosition > o.getRankingPosition()) {
            return 1;
        } else if (this.rankingPosition.equals(o.getRankingPosition())){
            return 0;
        }
        return -1;
    }
}
