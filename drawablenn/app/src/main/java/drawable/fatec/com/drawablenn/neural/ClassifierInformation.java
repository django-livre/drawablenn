package drawable.fatec.com.drawablenn.neural;

/**
 * Tipos classificados pela rede neural
 */
public class ClassifierInformation {

    public static final int CATEGORY_COUNT = 10;
    public static final String[] CLASSES = {
            "Avião", "Faca", "Dedo", "Escorpião", "Megafone", "Panda",
            "Relógio", "Donut", "Rádio", "Sol"
    };
}
