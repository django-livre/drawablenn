package drawable.fatec.com.drawablenn.game;

import android.graphics.Color;
import android.os.CountDownTimer;
import android.widget.TextView;

import java.util.Timer;

/**
 * A Game Timer to control the user game
 */
public class GameTime extends CountDownTimer {

    private int seconds;
    private Timer timer;
    private boolean isTimeout;
    private TextView time;

    public GameTime(int seconds, TextView time) {
        super(seconds * 1000, 1000);
        this.seconds = seconds;
        this.isTimeout = false;
        this.timer = new Timer();
        this.time = time;
    }

    public GameTime(int seconds) {
        super(seconds * 1000, 1000);
        this.isTimeout = false;
        this.timer = new Timer();
    }

    public void startTimer() {
        time.setTextColor(Color.BLACK);
        this.start();
        timer.schedule(new GameTimeAction(this),
                seconds * 1000);
    }

    public void changeTime(int seconds) {

        this.seconds = seconds;
        this.timer.cancel(); // Cancel all schedules
        this.timer.purge(); // Remove 13spall cancelled tasks
        this.timer = new Timer();
        this.isTimeout = false;

        startTimer();
    }

    public void setTimeout(boolean timeout) {
        this.isTimeout = timeout;
    }

    public boolean isTimeout() {
        return isTimeout;
    }

    @Override
    public void onTick(long remainingTime) {

        int currentTime = (int) (remainingTime / 1000) -1;
        if (currentTime < 6)
            time.setTextColor(Color.RED);
        time.setText("" + currentTime);
    }

    @Override
    public void onFinish() {
        time.setTextColor(Color.BLACK);
    }
}
