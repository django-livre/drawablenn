package drawable.fatec.com.drawablenn.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;

public class RankingResponse {

    @Expose
    @SerializedName("users")
    private HashMap<String, User> users;

    public HashMap<String, User> getUsers() {
        return users;
    }
}
