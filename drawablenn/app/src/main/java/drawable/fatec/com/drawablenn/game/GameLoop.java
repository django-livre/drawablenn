package drawable.fatec.com.drawablenn.game;

import android.graphics.Bitmap;
import android.util.Log;

import drawable.fatec.com.drawablenn.image.ImageProcessor;
import drawable.fatec.com.drawablenn.mvp.MVP;
import drawable.fatec.com.drawablenn.view.GameActivity;

/**
 * A Game Loop Engine to run the logic of Drawable game
 */
public class GameLoop extends Thread {

    private float FRAME_RATE = 3;
    private float FRAME_TIME = 1000 / FRAME_RATE;

    // private CanvasDrawable canvasDrawable;
    private GameActivity gameActivity;
    private MVP.PresenterGame presenterGame;

    public GameLoop(GameActivity gameActivity, MVP.PresenterGame presenterGame) {
        this.presenterGame = presenterGame;
        this.gameActivity = gameActivity;
    }

    @Override
    public void run() {
        while (!isInterrupted()) {

            float startTime = System.currentTimeMillis();

            if (!presenterGame.gameIsFinished() && presenterGame.playerIsAlive()) {

                if (presenterGame.isTimeout()) {

                    // removes the user's life and defines a new drawing
                    presenterGame.removeUserHeartPoint();
                    presenterGame.changeDrawClass();

                    gameActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            gameActivity.showErrorMessage();
                            gameActivity.clearCanvas();
                            gameActivity.changeCanvasText();
                            gameActivity.updateUsertHeart();
                            presenterGame.changeTime(15);
                        }
                    });

                    // Start a new timer
//                    presenterGame.changeTime(15);
                }

                try {
                    // This line return "Attempt to invoke virtual method 'int android.graphics.Bitmap.getWidth()' on a null object reference" on logcat
                    Bitmap bitmap = gameActivity
                            .getScaledBitmap(ImageProcessor.DIM_IMG_WIDTH, ImageProcessor.DIM_IMG_HEIGHT);
                    boolean betResult = presenterGame.classifyAndCheckUserDraw(bitmap);

                    if (betResult) {
                        presenterGame.changeDrawClass();
//                        presenterGame.changeTime(17);

                        // A simple way to show a success message
                        // clear canvas and update the drawing class
                        gameActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
// Change line 64 to ui thread for fix this issue "https://stackoverflow.com/questions/3875184/cant-create-handler-inside-thread-that-has-not-called-looper-prepare"
                                presenterGame.changeTime(17);
                                gameActivity.showSuccessMessage();
                            }
                        });

                        Thread.sleep(2 * 1000);
                        gameActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                gameActivity.clearCanvas();
                                gameActivity.changeCanvasText();
                            }
                        });
                        // this method probably will do the same thing reported in line 71
                        presenterGame.changeTime(15);
                    }
                } catch (Exception e) {
                    Log.e("GameLoopErrorGameLogic", e.getMessage());
                    e.printStackTrace();
                }

                float endTime = System.currentTimeMillis();
                long deltaTime = (long) (FRAME_TIME - (endTime - startTime));

                try {
                    // Solving the negative deltaTime bug
                    if (deltaTime < 0) {
                        Thread.sleep((deltaTime / 2) * -1);
                    } else {
                        Thread.sleep(deltaTime);
                    }
                } catch (InterruptedException e) {
                    return;
                }
            } else {
                gameActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        gameActivity.finishActivity();
                    }
                });
                return;
            }
        }
    }
}