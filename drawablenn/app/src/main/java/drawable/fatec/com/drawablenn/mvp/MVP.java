package drawable.fatec.com.drawablenn.mvp;

import android.graphics.Bitmap;
import android.view.View;

import java.util.Map;

import drawable.fatec.com.drawablenn.model.LoginResponse;
import drawable.fatec.com.drawablenn.model.PointResponse;
import drawable.fatec.com.drawablenn.model.ProfileResponse;
import drawable.fatec.com.drawablenn.model.RankingResponse;
import drawable.fatec.com.drawablenn.model.RegisterResponse;
import drawable.fatec.com.drawablenn.model.User;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface MVP {

    interface PresenterLogin {
        public void loginUser(User user);

        public void registerUser(User user);

    }

    interface PresenterRanking {
        public void seeRanking();
    }

    interface PresenterProfile {
        public void getUserProfile(User user);
    }

    interface PresenterGame {

        public void startGameTime();

        public void changeDrawClass();

        public void removeUserHeartPoint();

        public void changeTime(int seconds);

        public void savePoint();

        public boolean gameIsFinished();

        public boolean isTimeout();

        public boolean playerIsAlive();

        public boolean classifyAndCheckUserDraw(Bitmap bitmap);

        public int seeUserHeartPoint();

        public int seeUserTimeFreezePowerup();

        public String getActualDrawClass();
    }

    interface Model {
        /**
         * Método para a realização de login na API drawableAPI
         *
         * @param map
         * @return
         */
        @POST("login")
        Call<LoginResponse> loginUser(
                @QueryMap Map<String, String> map
        );

        /**
         * Método para a realização do cadastro no usuário na API drawabeAPI
         *
         * @param map
         * @return
         */
        @PUT("register")
        Call<RegisterResponse> registerUser(
                @QueryMap Map<String, String> map
        );

        /**
         * Método para recuperar o perfil do usuário
         *
         * @param userID
         * @return
         */
        @GET("profile")
        Call<ProfileResponse> userProfile(
                @Query("id") String userID
        );

        /**
         * Método para realizar a recuperação do Ranking de
         * pontos dos usuários.
         *
         * @return
         */
        @GET("ranking")
        Call<RankingResponse> fillRanking();

        /**
         * This method save user's point in the API DrawableNN, available at https://drawableapi.herokuapp.com/
         */
        @POST("point")
        Call<PointResponse> savePoint(
                @Query("id") String id,
                @Query("quantity") Integer quantity
        );

    }

    interface ViewPrincipalMenu {
        public void play(View view);

        public void seeRanking(View view);

        public void seeProfile(View view);

        public void quitApp(View view);

    }

    interface ViewLogin {
        public void login(View view);

        public void registerUser(View view);

        public void changeView();
    }

    interface ViewUserProfile {
        public void seeUserProfile();
    }

    interface ViewRanking {
    }

    interface ViewPlay {
        public void playGame(View view);
    }

    interface ViewRegistry {
        public void registerUser(View view);

        public void backToLogin();
    }
}
