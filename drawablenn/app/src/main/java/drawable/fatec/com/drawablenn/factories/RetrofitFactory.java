package drawable.fatec.com.drawablenn.factories;

import drawable.fatec.com.drawablenn.mvp.MVP;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitFactory {

    private static final String API_URL = "https://drawableapi.herokuapp.com";

    /**
     * Factory do Retrofit
     * @return
     */
    public static MVP.Model retrofitFactory() {
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(API_URL)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();
        return retrofit.create(MVP.Model.class);
    }
}
