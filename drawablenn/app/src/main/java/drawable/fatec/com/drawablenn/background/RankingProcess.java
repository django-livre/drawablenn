package drawable.fatec.com.drawablenn.background;

import android.os.AsyncTask;
import android.util.Log;

import drawable.fatec.com.drawablenn.factories.RetrofitFactory;
import drawable.fatec.com.drawablenn.model.ProfileResponse;
import drawable.fatec.com.drawablenn.model.RankingResponse;
import drawable.fatec.com.drawablenn.model.RegisterResponse;
import drawable.fatec.com.drawablenn.model.User;
import drawable.fatec.com.drawablenn.mvp.MVP;
import retrofit2.Call;
import retrofit2.Response;

public class RankingProcess extends AsyncTask<Void, Void, RankingResponse> {

    private MVP.Model retrofitModel;
    private RankingCallback rankingCallback;

    public RankingProcess(RankingCallback rankingCallback) {
        this.retrofitModel = RetrofitFactory.retrofitFactory();
        this.rankingCallback = rankingCallback;
    }

    @Override
    protected RankingResponse doInBackground(Void... voids) {
        RankingResponse rankingResponse = null;
        Call<RankingResponse> call = retrofitModel.fillRanking();

        try {
            Response executeResponse = call.execute();
            rankingResponse = (RankingResponse) executeResponse.body();
        } catch (Exception e) {
            Log.d("RankingError", e.getMessage());
        }
        return rankingResponse;
    }

    @Override
    protected void onPostExecute(RankingResponse rankingResponse ) {
        rankingCallback.fillRanking(rankingResponse);
    }
}
