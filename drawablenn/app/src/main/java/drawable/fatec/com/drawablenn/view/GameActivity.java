package drawable.fatec.com.drawablenn.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import org.zakariya.flyoutmenu.FlyoutMenuView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import drawable.fatec.com.drawablenn.R;
import drawable.fatec.com.drawablenn.background.PointCallback;
import drawable.fatec.com.drawablenn.game.GameLoop;
import drawable.fatec.com.drawablenn.model.PointResponse;
import drawable.fatec.com.drawablenn.model.User;
import drawable.fatec.com.drawablenn.mvp.MVP;
import drawable.fatec.com.drawablenn.presenter.FactualPresenterGame;
import es.dmoral.toasty.Toasty;

public class GameActivity extends AppCompatActivity implements PointCallback {


    @BindView(R.id.canvasDrawable)
    CanvasDrawable canvasDrawable;
    @BindView(R.id.myMenu)
    FlyoutMenuView smileyFlyoutMenu;
    @BindView(R.id.time)
    TextView time;

    private User user;
    private GameLoop gameLoop;
    private MVP.PresenterGame presenterGame;
    private int[] emojiCodes = {
            0x1F525 // Clear the canvas (Fire)
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_canvas);
        ButterKnife.bind(this);

        user = (User) getIntent().getSerializableExtra("user");
        // ToDo: Verify need to pass two this, and also, to recover user from activity
        presenterGame = new FactualPresenterGame(this, this, user, time);
        presenterGame.changeDrawClass();

        initializeFlyout();
        gameLoop = new GameLoop(this, presenterGame);
        canvasDrawable.setClassText(presenterGame.getActualDrawClass());
        canvasDrawable.setHeartPointsText(presenterGame.seeUserHeartPoint());
    }

    /**
     * Method to configure the Flyout menu
     */
    private void initializeFlyout() {
        // General color
        @ColorInt int color = ContextCompat.getColor(this, R.color.colorPrimaryDark);

        // Menu definitions
        float fontSizeInMenu = getResources().getDimension(R.dimen.palette_menu_item_size) * 0.5f;
        float fontSizeInButton = getResources().getDimension(R.dimen.flyout_menu_button_size) * 0.5f;
        List<EmojiFlyoutMenu.MenuItem> menuItems = new ArrayList<>();

        // Create a menu item to put in Flyout menu
        for (int code : emojiCodes) {
            menuItems.add(new EmojiFlyoutMenu.MenuItem(menuItems.size(),
                    code, fontSizeInMenu, color));
        }

        // assign a GridLayout with 2 columns and unspecified rows (allows menu to grow vertically)
        smileyFlyoutMenu.setLayout(new FlyoutMenuView.GridLayout(2,
                FlyoutMenuView.GridLayout.UNSPECIFIED));

        // assign the menuItems via an ArrayAdapter
        smileyFlyoutMenu.setAdapter(new FlyoutMenuView.ArrayAdapter<>(menuItems));

        // create and assign the button renderer. we'll change the button renderer's emoji in the callback below
        final EmojiFlyoutMenu.ButtonRenderer renderer = new EmojiFlyoutMenu.ButtonRenderer(
                emojiCodes[0], fontSizeInButton, color);
        smileyFlyoutMenu.setButtonRenderer(renderer);

        // Add the actions to the menu
        smileyFlyoutMenu.setSelectionListener(new FlyoutMenuView.SelectionListener() {
            @Override
            public void onItemSelected(FlyoutMenuView flyoutMenuView, FlyoutMenuView.MenuItem item) {
                EmojiFlyoutMenu.MenuItem emojiSelected = (EmojiFlyoutMenu.MenuItem) item;

                // Verify the action selected
                if (emojiSelected.getEmojiCode() == emojiCodes[0]) {
                    canvasDrawable.clearCanvas();
                }
                renderer.setEmojiCode(emojiSelected.getEmojiCode());
            }

            @Override
            public void onDismissWithoutSelection(FlyoutMenuView flyoutMenuView) {
            }
        });
    }

    @Override
    public void savePoint(PointResponse pointResponse) {
        if (pointResponse != null) {
            if (pointResponse.isSuccess()) {
                Toasty.success(this, "Seus pontos foram salvos com sucesso");
            } else {
                Toasty.warning(this, "Seus pontos não foram salvos");
            }
        } else {
            Toasty.error(this, "Não foi possível salvar seus pontos");
        }
    }

    /**
     * Method to finish the Game Activity and start the FinishGameActivity
     */
    public void finishActivity() {
        gameLoop.interrupt();
        canvasDrawable.setWillNotDraw(true);
        // A simple method to call the finish game window
        Intent finishActivity = new Intent(this, FinishGameActivity.class);
        finishActivity.putExtra("user", user);
        startActivity(finishActivity);
        finish();
    }

    /**
     * Clean the drawing canvas
     */
    public void clearCanvas() {
        canvasDrawable.clearCanvas();
    }

    /**
     * Method to change text in Canvas
     */
    public void changeCanvasText() {
        canvasDrawable.setClassText(presenterGame.getActualDrawClass());
    }

    /**
     * Method to update the User heart points in canvas
     */
    public void updateUsertHeart() {
        canvasDrawable.setHeartPointsText(presenterGame.seeUserHeartPoint());
    }

    /**
     * Method to get a scaled bitmap
     *
     * @param width
     * @param height
     * @return
     */
    public Bitmap getScaledBitmap(int width, int height) {
        return canvasDrawable.getScaledBitmap(width, height);
    }

    /**
     * Method to show a Successful message to user
     */
    public void showSuccessMessage() {
        Toasty.success(this, "Legal! Consegui entender seu desenho!",
                Toast.LENGTH_SHORT, true).show();
    }

    /**
     * Method to show a error message to user
     */
    public void showErrorMessage() {
        Toasty.error(this, "Ops! Acho que não entendi seu desenho...",
                Toast.LENGTH_SHORT, true).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenterGame.startGameTime();
        gameLoop.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        gameLoop.interrupt();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        gameLoop.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        gameLoop.interrupt();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        gameLoop.interrupt();
        gameLoop = null;
    }
}
