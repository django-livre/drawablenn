package drawable.fatec.com.drawablenn.background;

import android.os.AsyncTask;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import drawable.fatec.com.drawablenn.factories.RetrofitFactory;
import drawable.fatec.com.drawablenn.model.RegisterResponse;
import drawable.fatec.com.drawablenn.model.User;
import drawable.fatec.com.drawablenn.mvp.MVP;
import retrofit2.Call;
import retrofit2.Response;

public class RegisterProcess extends AsyncTask<User, Void, RegisterResponse> {

    private MVP.Model retrofitModel;
    private RegisterCallback registerCallback;

    public RegisterProcess(RegisterCallback registerCallback) {
        this.registerCallback = registerCallback;
        this.retrofitModel = RetrofitFactory.retrofitFactory();
    }

    @Override
    protected RegisterResponse doInBackground(User... users) {
        RegisterResponse registerResponse = null;
        Map<String, String> map = new HashMap<>();
        map.put("id", users[0].getId());
        map.put("password", users[0].getPassword());
        map.put("name", users[0].getName());
        map.put("email", users[0].getEmail());

        try {
            Call<RegisterResponse> call = retrofitModel.registerUser(map);
            Response executeResponse = call.execute();
            registerResponse = (RegisterResponse) executeResponse.body();
        } catch (Exception e) {
            Log.d("RegisterError", e.getMessage());
        }
        return registerResponse;
    }

    @Override
    protected void onPostExecute(RegisterResponse registerResponse) {
        registerCallback.doRegistry(registerResponse);
    }
}
