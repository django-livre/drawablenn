package drawable.fatec.com.drawablenn.view.util;

import android.widget.EditText;

/**
 * This class receive a vararg of EditText and verify if a empty label is present
 */
public class FormValidator {

    public static boolean validate(EditText... fields) {

        boolean areFieldsOk = true;

        for (EditText text : fields) {
            if ("".equals(text.getText().toString())) {
                text.setError("Campo obrigatório");
                areFieldsOk = false;
            }
        }

        return areFieldsOk;
    }

    ;

}
