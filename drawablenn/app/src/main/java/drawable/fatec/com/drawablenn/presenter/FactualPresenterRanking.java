package drawable.fatec.com.drawablenn.presenter;

import android.widget.ListView;

import butterknife.BindView;
import drawable.fatec.com.drawablenn.R;
import drawable.fatec.com.drawablenn.background.RankingCallback;
import drawable.fatec.com.drawablenn.background.RankingProcess;
import drawable.fatec.com.drawablenn.mvp.MVP;

public class FactualPresenterRanking implements MVP.PresenterRanking {

    private RankingCallback rankingCallback;

    public FactualPresenterRanking(RankingCallback rankingCallback) {
        this.rankingCallback = rankingCallback;
    }

    @Override
    public void seeRanking() {
        RankingProcess rankingProcess = new RankingProcess(rankingCallback);
        rankingProcess.execute();
    }
}
